// Copyright (c) 2014, The Shelf Route project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

import 'package:shelf/shelf.dart';
import 'package:shelf/shelf_io.dart' as io;
import 'package:shelf_route/shelf_route.dart';
//import 'package:shelf_exception_response/exception_response.dart';

class MyRouteCreator extends Routeable {
  void createRoutes(Router router) {
    router
      ..get('/', (_) => new Response.ok("Hello World"))
      ..get(
          '/greeting/{name}',
          (request) =>
              new Response.ok("Hello ${getPathParameter(request, 'name')}"));
  }
}

void main() {
  var rootRouter = router();

  rootRouter
    ..addAll(new MyRouteCreator())
    ..addAll(
        (Router r) => r
          ..get('/foo', (_) => new Response.ok('yeah foo'))
          ..get('/fum', (_) => new Response.ok('boo fum'))
          ..addAll(
              (Router r) => r
                ..get(
                    '/yup/{title}',
                    (req) => new Response.ok(
                        "Hello ${getPathParameter(req, 'name')}"))
                ..get(
                    '/nup',
                    (req) => new Response.ok(
                        "Hello ${getPathParameter(req, 'name')}")),
              path: '/blah/{name}'),
        path: '/bar');

  var handler = const Pipeline()
      .addMiddleware(logRequests())
//      .addMiddleware(exceptionResponse())
      .addHandler(rootRouter.handler);

  printRoutes(rootRouter);

  io.serve(handler, 'localhost', 8080).then((server) {
    print('Serving at http://${server.address.host}:${server.port}');
  });
}
