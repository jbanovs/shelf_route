// Copyright (c) 2015, The Shelf Route project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

/// The shelf_route library.
///
/// The shelf_route library contains functions and classes to help you easily build
/// your routes. Typical usage is to use the [router] function to create
/// your [Router] and then use the methods on [Router] to add your routes.
///
/// For example
///
///     import 'package:shelf/shelf.dart';
///     import 'package:shelf/shelf_io.dart' as io;
///     import 'package:shelf_route/shelf_route.dart' as r;
///     import 'package:shelf_exception_handler/shelf_exception_handler.dart';
///
///     var router = r.router()
///       ..post('/', (_) => new Response.ok("Hello World"))
///       ..delete('/boo', (_) => new Response.ok("Bye cruel world"),
///           middleware: logRequests())
///       ..add('/multimethod', ['GET', 'PUT'], (_) => new Response.ok("Howdy World"))
///       ..get('/greeting/{name}{?age}', (request) {
///         var name = r.getPathParameter(request, 'name');
///         var age = r.getPathParameter(request, 'age');
///         return new Response.ok("Hello $name of age $age");
///       });
///
///     var handler = const Pipeline()
///         .addMiddleware(logRequests())
///         .addMiddleware(exceptionHandler())
///         .addHandler(router.handler);
///
///     r.printRoutes(router);
///
///     io.serve(handler, 'localhost', 8080).then((server) {
///       print('Serving at http://${server.address.host}:${server.port}');
///     });

library shelf_route;

export 'package:shelf_path/shelf_path.dart'
    show getPathParameters, getPathParameter;

export 'src/api.dart';
export 'src/core.dart';
export 'src/route.dart';
export 'src/default_segment_router.dart';
export 'src/default_route.dart';
export 'src/router_builder.dart';
export 'src/routing_context.dart';
