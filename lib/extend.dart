// Copyright (c) 2015, The Shelf Route project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

/// The shelf_route.extend library.
///
/// shelf_route is designed to be both easy to use out of the box as well as
/// highly extensible for other libraries to build their own routing APIs on top
/// of.
///
/// The shelf_route.extend library exposes the internal implementations that
/// support that extension
///
/// # Architecture #
/// `shelf_route` is broken into two main parts:
///
/// 1. The core routing components such as [Route], [SimpleRoute] and
/// [RequestRouter]. These are immutable components that perform the actual
/// routing of requests.
/// 1. The router builder components such as [SpecBasedRouterBuilder] and
/// [DefaultRouterBuilder]. These are responsible for building the runtime
/// components (Route etc)
///
/// ## Router Builders ##
/// Corresponding to the runtime routing components are pairs of more abstract
/// models. These pairs are an abstract representation of a route called a
/// route spec and an adapter that is responsible for creating the corresponding
/// route component from the given route spec. More specifically there are:
///
/// * [SimpleRouteSpec] and [SimpleRouteAdapter] which produce [SimpleRoute]
/// * [RouterSpec] and [RouterAdapter] which produce [RequestRouter]
/// * [RouteSpec] and [RouteAdapter] which are the root of the hierarchy
/// corresponding to [Route]
///
/// Note these models a deliberately very abstract to support the most flexibility
/// possible. However, in almost all circumstances you are more likely to deal
/// with subclasses like [DefaultRouterAdapter] that provide more concrete
/// implementations supporting ways to provide [Middleware] and adapting
/// route paths (e.g. supporting different path styles like ':foo') and handlers
/// (such as that provided by shelf_bind that allows normal Dart functions to
/// be used as shelf handlers)
///
/// The most advanced form of extensions to shelf_route typically work at this
/// level but producing specs and adapters, either or both of which may be
/// custom subclasses.
///
/// Note the Adapters inherit properties from parent routes. So often it is not
/// necessary to provide an adapter at each node in the routing tree. A single
/// one at the top of the tree may be enough.
///
/// [SpecBasedRouterBuilder], which is also a router spec, has methods
/// to add these specs to builder, such as [addRoute]
///
/// ## Common Extensions ##
/// Only the most complex extensions require providing your own [RouteSpec] and
/// [RouteAdapter] implementations.
///
/// Most extensions are much simpler. The following are some common ways to
/// extend `shelf_route`
///
/// Note: All of these can be provided to the top level `router` function as
/// well as to all the methods of [DefaultRouterBuilder]
///
/// Note also that all these extension mechanisms are inherited down the route tree so
/// you only need to provide it a the level you wish it to take affect.
///
/// ### Custom Handler Adapters
/// If you don't want to always have to deal directly with the `shelf` [Request]
/// and [Response] objects, then a custom [HandlerAdapter] can be provided.
///
/// [shelf_bind](https://pub.dartlang.org/packages/shelf_bind) provides a
/// custom [HandlerAdapter] that allows you to use any Dart function as a
/// [Handler]
///
/// ### Extend [Routeable]
/// [DefaultRouterBuilder#addAll] takes a function of type [RouteableFunction].
/// You can implement this function for your integration and use the normal
/// methods on [DefaultRouterBuilder] to create your routes
///
/// ### Custom [PathAdapter]s
/// By default `shelf_route` uses the [uri](https://pub.dartlang.org/packages/uri)
/// package to specify the paths in the routes. This is very flexible and
/// powerful but if it doesn't meet your needs you can implement [PathAdapter]
/// to use your own
///
/// ### Middleware
/// Many things can be accomplished by simply adding your own [Middleware]
/// at the appropriate points in your route hierarchy
///
///
library shelf_route.extend;

export 'shelf_route.dart';
export 'src/api_impl.dart';
export 'src/route_impl.dart';
export 'src/default_adapters.dart';
export 'src/default_segment_router.dart';
export 'src/default_route_impl.dart';
export 'src/router_builder_impl.dart';
export 'src/segment_router.dart';
