// Copyright (c) 2015, The Shelf Route project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_route.router.fundamentals.impl;

import 'route.dart';
import 'package:shelf/shelf.dart';
import 'package:option/option.dart';
import 'core.dart';
import 'dart:async';
import 'segment_router.dart';
import 'package:quiver/iterables.dart';

abstract class _BaseRoute<S extends SegmentRouter> implements Route {
  final String name;
  final S segmentRouter;
  final Handler handler;

  _BaseRoute(this.name, this.segmentRouter, this.handler);

  bool canHandle(Request request) => segmentRouter.canHandle(request);

  @override
  Future<Response> handle(Request request) =>
      segmentRouter.handle(request, handler, this);

  Handler createHandler(Handler fallbackHandler) {
    return (Request request) async {
      try {
        return await handle(request);
      } on NotHandled {
        return fallbackHandler(request);
      }
    };
  }

  String toString() => '$runtimeType at $segmentRouter';
}

class SimpleRouteImpl<S extends SegmentRouter> extends _BaseRoute<S>
    implements SimpleRoute {
  SimpleRouteImpl(String name, S segmentRouter, Handler handler)
      : super(name, segmentRouter, handler);

  void accept(RouteVisitor visitor) {
    visitor.visitSimple(this);
  }

  Iterable<SimpleRoute> get flattened => [this];

  @override
  Option<RoutePath> findByName(String name) =>
      name == this.name ? new Some(createRoutePath()) : const None();

  RoutePath createRoutePath() => new RoutePathImpl(const [], this);
}

class RoutePathImpl<R extends Route> implements RoutePath<R> {
  final Iterable<RequestRouter<R>> parents;
  final R route;

  RoutePathImpl(this.parents, this.route);
}

class RequestRouterImpl<S extends SegmentRouter, R extends Route>
    extends _BaseRoute<S> implements RequestRouter<R> {
  final Iterable<R> routes;

  RequestRouterImpl._(
      String name, S segmentRouter, Handler handler, this.routes)
      : super(name, segmentRouter, handler);

  RequestRouterImpl(String name, S segmentRouter, Iterable<Route> routes,
      Option<Middleware> middlewareOpt)
      : this._(
            name, segmentRouter, _createHandler(middlewareOpt, routes), routes);

  @override
  Iterable<SimpleRoute> get flattened => routes.expand((r) => r.flattened).map(
      (SimpleRouteImpl childRoute) => new SimpleRouteImpl(name,
          segmentRouter.join(childRoute.segmentRouter), childRoute.handler));

  @override
  Iterable<PathInfo> get flattenedPaths {
    return flattened.map((SimpleRoute route) {
      if (route is SimpleRouteImpl) {
        return route.segmentRouter.pathInfo;
      } else {
        throw new StateError('ouch. Really gotta fix this');
      }
    });
  }

  void accept(RouteVisitor visitor) {
    routes.forEach((r) {
      r.accept(visitor);
    });
    visitor.visitRouter(this);
  }

  String toString() =>
      '$runtimeType at $segmentRouter; # routes ${routes.length}';

  static Handler _createHandler(
      Option<Middleware> middlewareOpt, Iterable<Route> routes) {
    final routerHandler = (Request request) {
      final handlingRoute = routes.firstWhere(
          (route) => route.canHandle(request),
          orElse: () => throw new NotHandled());
      return handlingRoute.handle(request);
    };

    return middlewareOpt
        .map((middleware) =>
            new Pipeline().addMiddleware(middleware).addHandler(routerHandler))
        .getOrElse(() => routerHandler);
  }

  @override
  Option<RoutePath> findByName(String name) {
    if (name == this.name) {
      return new Some(createRoutePath(const [], this));
    } else {
      return routes
          .map((Route r) => r.findByName(name))
          .firstWhere((o) => o is Some, orElse: () => const None())
          .map((RoutePath rp) => createRoutePath(
              concat([
                [this],
                rp.parents
              ]),
              rp.route));
    }
  }

  RoutePath createRoutePath(Iterable<RequestRouter<R>> parents, Route route) =>
      new RoutePathImpl(parents, route);
}
