// Copyright (c) 2015, The Shelf Route project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_route.core;

import 'package:shelf/shelf.dart';
import 'package:uri/src/uri_pattern.dart';
import 'router_builder.dart';

class PathInfo {
  final String method;
  final String path;

  PathInfo(String method, String path)
      : this.method = method != null ? method : '*',
        this.path = path != null && !path.isEmpty ? path : '/';

  String toString() => '$method -> $path';
}

/// A function capable of creating routes in a given [Router]
typedef RouteableFunction(DefaultRouterBuilder router);

/// a function that adapts a function to be a shelf [Handler]
typedef Handler HandlerAdapter(Function handler);

/// a function that adapts a function to be a [RouteableFunction]
typedef RouteableFunction RouteableAdapter(Function handler);

/// A [HandlerAdapter] that can be merged with another handlerAdapter of the same
/// type
abstract class MergeableHandlerAdapter {
  HandlerAdapter merge(MergeableHandlerAdapter other);
}

/// a function that adapts some representation of a path into the form
/// [UriPattern] that is used internally
typedef UriPattern PathAdapter(path);

//typedef Route RouteAdapter(route);
//typedef Router RouterAdapter(router);
//

/// a HandlerAdapter that does nothing
Handler noopHandlerAdapter(Handler handler) => handler;

/// a RouteableFunction that does nothing
RouteableFunction noopRouteableAdapter(RouteableFunction routeable) =>
    routeable;
