// Copyright (c) 2015, The Shelf Route project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_route.router.adapters;

import 'package:shelf/shelf.dart';
import 'package:uri/uri.dart';
import 'package:path/path.dart' as p;
import 'router_builder.dart';
import 'core.dart';
import 'package:option/option.dart';
import 'default_route.dart';
import 'default_route_impl.dart';
import 'default_segment_router.dart';
import 'router_builder_impl.dart';

final _url = p.url;

class DefaultSimpleRouteAdapter extends BaseRouteAdapter<DefaultSimpleRoute>
    implements SimpleRouteAdapter {
  const DefaultSimpleRouteAdapter(
      HandlerAdapter handlerAdapter, PathAdapter pathAdapter)
      : super(handlerAdapter, pathAdapter);

  const DefaultSimpleRouteAdapter.def()
      : this(noopHandlerAdapter, uriTemplatePattern);

  DefaultSimpleRoute adapt(SimpleRouteSpec route) {
    return new DefaultSimpleRouteImpl(
        route.name,
        new DefaultSegmentRouter(
            pathAdapter(route.path), route.methods, route.exactMatch),
        _adaptHandler(route.handler, route.middleware));
  }

  Handler _adaptHandler(handler, Middleware middleware) {
    final adapted = handlerAdapter(handler);
    return middleware != null
        ? new Pipeline().addMiddleware(middleware).addHandler(adapted)
        : adapted;
  }

  @override
  DefaultSimpleRouteAdapter create(HandlerAdapter mergedHandlerAdapter,
      PathAdapter mergedPathAdapter, DefaultRouterAdapter other) {
    return new DefaultSimpleRouteAdapter(
        mergedHandlerAdapter, mergedPathAdapter);
  }
}

class DefaultRouterAdapter extends BaseRouteAdapter<DefaultRouter>
    implements RouterAdapter {
  const DefaultRouterAdapter(
      HandlerAdapter handlerAdapter, PathAdapter pathAdapter)
      : super(handlerAdapter, pathAdapter);

  const DefaultRouterAdapter.def(
      {HandlerAdapter handlerAdapter: noopHandlerAdapter,
      PathAdapter pathAdapter: uriTemplatePattern})
      : this(handlerAdapter, pathAdapter);

  DefaultRouter adapt(RouterSpec router) {
    final routes = router.routes.map(_adaptChild).toList();
    return new DefaultRouterImpl(
        router.name,
        new DefaultSegmentRouter(pathAdapter(router.path), null, false),
        routes,
        new Option(router.middleware));
  }

  DefaultRoute _adaptChild(RouteSpec route) {
    final routeAdapter = route.routeAdapter.merge(this);
    return routeAdapter.adapt(route);
  }

  @override
  DefaultRouterAdapter create(HandlerAdapter mergedHandlerAdapter,
      PathAdapter mergedPathAdapter, DefaultRouterAdapter other) {
    return new DefaultRouterAdapter(mergedHandlerAdapter, mergedPathAdapter);
  }
}

class DefaultRouterBuilderAdapter extends DefaultRouterAdapter {
  final RouteableAdapter routeableAdapter;

  const DefaultRouterBuilderAdapter(HandlerAdapter handlerAdapter,
      PathAdapter pathAdapter, this.routeableAdapter)
      : super(handlerAdapter, pathAdapter);

  const DefaultRouterBuilderAdapter.def(
      {HandlerAdapter handlerAdapter: noopHandlerAdapter,
      PathAdapter pathAdapter: uriTemplatePattern,
      RouteableAdapter routeableAdapter})
      : this(handlerAdapter, pathAdapter, routeableAdapter);

  DefaultRouter adapt(RouteableRouterSpec router) {
    // apply the routeable first if one exists. Any routes created by the
    // routeable will then be potentially further adapted
    if (router.routeable is Some) {
      final routeableFunction = routeableAdapter(router.routeable.get());
      routeableFunction(router);
    }

    return super.adapt(router);
  }

  @override
  DefaultRouterBuilderAdapter create(HandlerAdapter mergedHandlerAdapter,
      PathAdapter mergedPathAdapter, DefaultRouterAdapter other) {
    final ra = _default(
        this.routeableAdapter != null
            ? this.routeableAdapter
            : other is DefaultRouterBuilderAdapter
                ? other.routeableAdapter
                : null,
        noopRouteableAdapter);

    return new DefaultRouterBuilderAdapter(
        mergedHandlerAdapter, mergedPathAdapter, ra);
  }
}

_default(value, defaultValue) => value != null ? value : defaultValue;

abstract class BaseRouteAdapter<R extends DefaultRoute>
    implements RouteAdapter<R> {
  final HandlerAdapter _handlerAdapter;
  final PathAdapter _pathAdapter;

  HandlerAdapter get handlerAdapter =>
      _default(_handlerAdapter, noopHandlerAdapter);
  PathAdapter get pathAdapter => _default(_pathAdapter, uriTemplatePattern);

  const BaseRouteAdapter(this._handlerAdapter, this._pathAdapter);

  @override
  RouteAdapter<R> merge(DefaultRouterAdapter other) {
    return create(
        maybeMergeHandlerAdapters(other.handlerAdapter),
        _default(
            this._pathAdapter != null ? this._pathAdapter : other.pathAdapter,
            uriTemplatePattern),
        other);
  }

  RouteAdapter<R> create(HandlerAdapter mergedHandlerAdapter,
      PathAdapter mergedPathAdapter, DefaultRouterAdapter other);

  HandlerAdapter maybeMergeHandlerAdapters(HandlerAdapter handlerAdapter) {
    var ha = null;
    if (this._handlerAdapter == null) {
      ha = handlerAdapter;
    } else if (handlerAdapter == null) {
      ha = this.handlerAdapter;
    } else if (handlerAdapter is MergeableHandlerAdapter &&
        this._handlerAdapter is MergeableHandlerAdapter) {
      MergeableHandlerAdapter haChild =
          this._handlerAdapter as MergeableHandlerAdapter;
      MergeableHandlerAdapter haParent =
          handlerAdapter as MergeableHandlerAdapter;

      ha = haChild.merge(haParent);
    } else {
      ha = this.handlerAdapter;
    }

    return ha;
  }
}

UriPattern uriTemplatePattern(path) {
  if (path == null) {
    return null;
  } else if (path is UriPattern) {
    return path;
  } else if (path is String) {
    // Note: we change '/' to '' as UriParser strips trailing '/'
    // and a '' path won't match against a pattern of '/'
    final adjustedPath = path.startsWith('/') ? path.substring(1) : path;
    return new UriParser(new UriTemplate(adjustedPath),
        queryParamsAreOptional: true);
  } else {
    throw new ArgumentError.value('path must be a String');
  }
}
