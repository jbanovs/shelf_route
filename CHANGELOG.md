## 0.14.3

- handle null values for path parameters

## 0.14.2

- updated some dependency versions

## 0.14.1

* bug fix: don't use decoded path parameters when creating new Request objects

## 0.14.0

* query parameters are now optional

## 0.13.0

* the internals have been completely rewritten to improve modularity, flexibility,
  extensibility, testability and a handful of other ilities. Mostly, this should
  be transparent to users, except for the noted breaking changes below. 
* removed `attach` method from Router
* Dependency versions updated:
  * matcher to <0.12.0
  * uri to <0.10.0
* Deprecated unittest module replaced with test

## 0.11.0+1

* Added complete hello world example to README

## 0.11.0

* Added RouteableAdapter to make it cleaner to use packages like shelf_rest

## 0.10.1

* split into API and implementation
* exposed a new library (extend.dart) for those wishing to subclass Router

## 0.10.0

* added support for mergeable handler adapters. This allows things like
customObjects to be extended by child routes

Note: this is only backward incompatible if you rely on handlerAdapters not
being merged. i.e. to override instead

## 0.9.0

* Support for Middleware directly on Router (i.e. can now add at top level)
* Unified Router and _Route via a composite pattern

Note: should be backward compatible with 0.8.x unless you depended on deprecated
methods that have been removed

## 0.8.0+1

* Don't consume path when exact match is false

## 0.8.0

* Changed `Router.addAll` to take a typedef rather than a class
    * This is backwards incompatible only for anyone that has a class that 
    `implements Routeable`. To fix this simply use `extends Routeable` instead.
    
This change makes it easier to set up hierarchical routes. e.g.

```
var myRouter = router()
      ..addAll((Router r) => r
        ..get('/foo', (_) =>  new Response.ok('yeah foo'))
        ..get('/fum', (_) =>  new Response.ok('boo fum'))
        ..addAll((Router r) => r
            ..get('/yup/{title}', 
                (req) => new Response.ok("Hello ${getPathParameter(req, 'name')}"))
            ..get('/nup', 
                (req) => new Response.ok("Hello ${getPathParameter(req, 'name')}")),
          path: '/blah/{name}'),
        path: '/bar'
    );

```
    

## 0.7.1

* Added fallbackHandler to router function
* call handlerAdapter on fallbackHandler
* added ALL_METHODS constant

## 0.7.0+1

* Fixed use of Deprecated

## 0.7.0

* Fixed spelling of Adapter in several classes (thanks Michael Goodness for the
contribution)

## 0.6.0
Major release with lots of changes including:

* custom path adapters
  * Allows you to plugin any path style you like (e.g. `/:foo`)
* custom handler adapters
  * Seamless integration with other Shelf components like Shelf Bind so you can use ordinary Dart functions as Shelf Handlers. 
  For example:  
```
  var myRouter = router(handlerAdapter: bind.handlerAdapter)
  myRouter.get('/{name}', (name) => "Hello ${name}");
```
* Support for Shelf Path to remove direct dependency of handlers to Shelf Route
* Easy to add per route middleware
* print all routes in a router
* control over exact path matching
* Fixed path handling on windows


## 0.5.0

* Upgraded to shelf 0.5.1. Note this is only a breaking change for those that
directly accessed the path variables in the header. Unfortunately the example
and doco did this so I made this a major release 

## 0.4.0

* Upgraded to shelf 0.5.0

## 0.3.1

* Support for custom [UriPattern](http://pub.dartlang.org/packages/uri)'s 

## 0.3.0

* Use [UriPattern and UriTemplate](http://pub.dartlang.org/packages/uri) 
instead of custom path matching

    * path variables now use `{}` instead of `:`

## 0.2.0+1

* Fixed handling of routes that are just a path variable
 
## 0.2.0

* Upgraded to shelf 0.4.0 

## 0.1.2+1

* Fixed bug where body was missing on copied request 

## 0.1.2

* Added helper function `getPathVariables` to encapsulate extracting path 
 variables stored in the request

## 0.1.1

* Added support for path variables.

* Improved tests.
